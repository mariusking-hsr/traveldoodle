# Traveldoodle

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test:once` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## CI/CD

Every pushed commit to the repository triggers a pipeline build on [bitbucket](https://bitbucket.org/mariusking-hsr/traveldoodle/addon/pipelines/home#!/). 
Every successful master build will be published on [https://travel-doodle.firebaseapp.com/](https://travel-doodle.firebaseapp.com/).

## Firebase
Firebase as Authentication Provider
Cloud Firestore as Backend Service

## Test Classes
- survey-resolver.service.spec.ts
- survey-evaluation.service.spec.ts
- survey-add.component.spec.ts
