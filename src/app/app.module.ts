import {environment} from '../environments/environment';

import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ErrorHandler, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSnackBarModule
} from '@angular/material';
import {ClipboardModule} from 'ngx-clipboard';
import {CalendarModule} from 'primeng/primeng';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import {AgmCoreModule} from '@agm/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './home/home.component';
import {SurveyComponent} from './survey/survey.component';
import {SurveyAddComponent} from './survey/survey-add/survey-add.component';
import {SurveyEvaluationComponent} from './survey/survey-evaluation/survey-evaluation.component';
import {SurveyVoteComponent} from './survey/survey-vote/survey-vote.component';
import {HeaderComponent} from './header/header.component';
import {ContentComponent} from './shared/components/content/content.component';
import {SigninComponent} from './auth/signin/signin.component';
import {SignupComponent} from './auth/signup/signup.component';
import {ErrorHandlerService} from './shared/error/error-handler.service';
import {DateRangeSelectorComponent} from './shared/components/form/date-range-selector/date-range-selector.component';
import {PlacesAutocompleteComponent} from './shared/components/form/places-autocomplete/places-autocomplete.component';
import {CommentsComponent} from './comments/comments.component';
import {DeleteableIconOptionComponent} from './shared/components/form/deleteable-icon-option/deleteable-icon-option.component';
import {SelectableIconOptionComponent} from './shared/components/form/selectable-icon-option/selectable-icon-option.component';
import {OptionGridComponent} from './shared/components/form/option-grid/option-grid.component';
import {LinkSharerComponent} from './shared/components/link-sharer/link-sharer.component';
import {FormActionComponent} from './shared/components/form/form-action/form-action.component';
import {CommentFormComponent} from './comments/comment-form/comment-form.component';
import {CommentComponent} from './comments/comment/comment.component';
import {InfoDialogComponent} from './home/info-dialog/info-dialog.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {LabelWithIconComponent} from './shared/components/label-with-icon/label-with-icon.component';
import {faInfoCircle} from '@fortawesome/free-solid-svg-icons/faInfoCircle';
import {faGlobeEurope} from '@fortawesome/free-solid-svg-icons/faGlobeEurope';
import {faArchway} from '@fortawesome/free-solid-svg-icons/faArchway';
import {faTrain} from '@fortawesome/free-solid-svg-icons/faTrain';
import {faRoad} from '@fortawesome/free-solid-svg-icons/faRoad';
import {faCocktail} from '@fortawesome/free-solid-svg-icons/faCocktail';
import {faUmbrellaBeach} from '@fortawesome/free-solid-svg-icons/faUmbrellaBeach';
import {faSpa} from '@fortawesome/free-solid-svg-icons/faSpa';
import {faShip} from '@fortawesome/free-solid-svg-icons/faShip';
import {faHiking} from '@fortawesome/free-solid-svg-icons/faHiking';
import {faCampground} from '@fortawesome/free-solid-svg-icons/faCampground';
import {faSkiing} from '@fortawesome/free-solid-svg-icons/faSkiing';
import {faGolfBall} from '@fortawesome/free-solid-svg-icons/faGolfBall';
import {faMapMarker} from '@fortawesome/free-solid-svg-icons/faMapMarker';
import {faCalendar} from '@fortawesome/free-solid-svg-icons/faCalendar';
import {faWhatsapp} from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import {faShare} from '@fortawesome/free-solid-svg-icons/faShare';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {AlreadyVotedDialogComponent} from './survey/survey-vote/already-voted-dialog/already-voted-dialog.component';
import {EvaluatedOptionComponent} from './survey/survey-evaluation/evaluated-option/evaluated-option.component';
import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SurveyAddComponent,
    SurveyEvaluationComponent,
    SurveyVoteComponent,
    HeaderComponent,
    ContentComponent,
    SigninComponent,
    SignupComponent,
    SurveyComponent,
    DateRangeSelectorComponent,
    PlacesAutocompleteComponent,
    CommentsComponent,
    DeleteableIconOptionComponent,
    SelectableIconOptionComponent,
    OptionGridComponent,
    LinkSharerComponent,
    FormActionComponent,
    CommentFormComponent,
    CommentComponent,
    InfoDialogComponent,
    LabelWithIconComponent,
    AlreadyVotedDialogComponent,
    EvaluatedOptionComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({apiKey: environment.googleplaces, libraries: ['places']}),
    MatGoogleMapsAutocompleteModule.forRoot(),
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatButtonToggleModule,
    MatDividerModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatListModule,
    MatChipsModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    ClipboardModule,
    AppRoutingModule,
    CalendarModule,
    FontAwesomeModule
  ],
  providers: [
    {provide: ErrorHandler, useClass: ErrorHandlerService}
  ],
  entryComponents: [
    InfoDialogComponent,
    AlreadyVotedDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    library.add(
      faInfoCircle,
      faRoad,
      faTrain,
      faArchway,
      faGlobeEurope,
      faCocktail,
      faUmbrellaBeach,
      faSpa,
      faShip,
      faHiking,
      faCampground,
      faSkiing,
      faGolfBall,
      faTrash,
      faShare,
      faWhatsapp,
      faCalendar,
      faMapMarker
    );
  }
}
