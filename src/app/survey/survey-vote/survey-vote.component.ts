import {Component, OnDestroy, OnInit} from '@angular/core';
import {SurveyService} from '../survey.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SurveyModel} from '../survey.model';
import {Observable, Subscription} from 'rxjs';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';
import {ResponseModel} from '../response.model';
import {ResponseService} from '../response.service';
import {User} from 'firebase';
import {AlreadyVotedDialogComponent} from './already-voted-dialog/already-voted-dialog.component';
import {MatDialog} from '@angular/material';
import {first, map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-survey-vote',
  templateUrl: './survey-vote.component.html',
  styleUrls: ['./survey-vote.component.scss']
})

export class SurveyVoteComponent implements OnInit, OnDestroy {

  public survey$: Observable<SurveyModel>;
  public currentUser$: Observable<User>;

  private surveyId: string;
  private surveyResponse;
  private subscription: Subscription;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private surveyService: SurveyService,
    private responseService: ResponseService,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.surveyResponse = {};
    this.currentUser$ = this.authService.currentUser$;

    this.survey$ = this.route.data.pipe(
      map(({survey}) => survey),
      tap(survey => this.surveyId = survey.id)
    );

     this.subscription = this.authService.currentUser$.subscribe((user: User) => {
        if (user) {
          this.surveyResponse.email = user.email;
        }
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public onSubmit({value}: NgForm) {
    const surveyResponse: ResponseModel = Object.assign(this.surveyResponse, value);

    this.responseService.hasUserAlreadyVoted(this.surveyId, this.surveyResponse.email)
      .pipe(first())
      .subscribe((voted) => {
          if (voted) {
            this.handleAlreadyVoted(this.surveyId, surveyResponse);
          } else {
            this.vote(this.surveyId, surveyResponse);
          }
        }
      );
  }

  private handleAlreadyVoted(surveyId: string, surveyResponse: ResponseModel) {
    const dialogRef = this.dialog.open(AlreadyVotedDialogComponent);

    dialogRef.afterClosed().subscribe(voteAgain => {
      if (voteAgain) {
        this.vote(surveyId, surveyResponse);
      }
    });
  }

  private vote(surveyId: string, surveyResponse: ResponseModel) {
    this.responseService.add(surveyId, surveyResponse)
      .pipe(first())
      .subscribe(() => this.navigateToEvalPage(surveyId));
  }

  private navigateToEvalPage(surveyId: String) {
    this.router.navigate([`survey/${surveyId}/eval`]);
  }
}
