import {Injectable} from '@angular/core';
import {RESPONSE, ResponseModel} from './response.model';
import {Observable} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {SURVEY} from './survey.model';
import {AngularFirestore} from '@angular/fire/firestore';
import {IdService} from '../shared/utils/id.service';
import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;
import {first, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  constructor(private readonly afs: AngularFirestore,
              private idService: IdService) {
  }

  public add(surveyId: string, response: ResponseModel): Observable<void> {
    response.id = this.idService.generateId();
    response.createdAt = Timestamp.now();

    return fromPromise(
      this.afs.collection<ResponseModel>(`${SURVEY}/${surveyId}/${RESPONSE}`).doc(response.id).set(response)
    );
  }

  public all(surveyId: string): Observable<ResponseModel[]> {
    const collection = this.afs.collection<ResponseModel>(`${SURVEY}/${surveyId}/${RESPONSE}`);
    return collection.valueChanges();
  }

  public hasUserAlreadyVoted(surveyId: string, email: String): Observable<boolean> {
    const collection = this.afs.collection<ResponseModel>(`${SURVEY}/${surveyId}/${RESPONSE}`,
      ref => ref.where('email', '==', email));
    return collection.valueChanges()
      .pipe(
        first(),
        map(responses => responses.length > 0)
      );
  }
}
