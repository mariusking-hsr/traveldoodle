import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;

export class SurveyModel {
  id: string;
  title: string;
  description: string;
  dates: DateOption[];
  locations: LocationOption[];
  types: TypeOption[];
  userId: string;
  createdAt: Timestamp;
}

export class Option {
  id: string;
}

export class SelectableOption extends Option {
  selected: boolean;
}

export class DateOption extends Option {
  from: Date;
  to: Date;
}

export class LocationOption extends Option {
  location: string;
}

export class TypeOption extends SelectableOption {
  type: string;
  icon: string;
}

export const SURVEY = 'surveys';
