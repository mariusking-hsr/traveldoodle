import {Injectable} from '@angular/core';
import {SurveyModel} from './survey.model';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {SurveyService} from './survey.service';
import {first, flatMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SurveyResolverService implements Resolve<SurveyModel> {

  constructor(private surveyService: SurveyService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SurveyModel> {
    const id = route.paramMap.get('id');

    return this.surveyService.get(id).pipe(
      first(),
      flatMap(survey => {
        if (survey) {
          return of(survey);
        } else { // id not found
          this.router.navigate(['/not-found']);
          return EMPTY;
        }
      })
    );
  }


}
