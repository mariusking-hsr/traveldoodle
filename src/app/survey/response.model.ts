import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;

export interface ResponseModel {
  id: string;
  selectedDates: string[];
  selectedLocations: string[];
  selectedTypes: string[];
  email: string;
  createdAt: Timestamp;
}

export const RESPONSE = 'responses';
