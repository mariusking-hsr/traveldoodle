import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-evaluated-option',
  templateUrl: './evaluated-option.component.html',
  styleUrls: ['./evaluated-option.component.scss']
})
export class EvaluatedOptionComponent {
  @Input()
  evaluatedOption;

  @Input()
  icon: string;

  public toggleVoters(option) {
    option.showVoter = !option.showVoter && option.voters.length > 0;
  }
}
