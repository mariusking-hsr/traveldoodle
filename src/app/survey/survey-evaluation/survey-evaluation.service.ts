import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SurveyEvaluationService {

  constructor() { }

  private static calcOptionStyle(currentResponsesTotal, responsesTotal) {
    // tslint:disable-next-line max-line-length
    return {'background-image': `repeating-linear-gradient(90deg,rgba(0,0,0,.1) 0%,rgba(0,0,0,.1) ${100 / responsesTotal * currentResponsesTotal}%,transparent 0%,transparent 100%)`};
  }

  public eval(options, responses, type) {
    return options.map(option => {
      const r = responses.filter(response => response[type].includes(option.id));

      return {
        option,
        total: responses.length,
        voters: r.map(re => re.email),
        showVoter: false,
        style: SurveyEvaluationService.calcOptionStyle(r.length, responses.length)
      };
    });
  }
}
