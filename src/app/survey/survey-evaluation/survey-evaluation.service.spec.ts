import {SurveyEvaluationService} from './survey-evaluation.service';
import {OPTIONS, RESPONSES, TYPE} from '../../test/test-fake-objects';

describe('SurveyEvaluationService', () => {



  let service: SurveyEvaluationService;

  beforeEach(() => {
    service = new SurveyEvaluationService();
  });

  it('there should be as many object in the result than options', () => {
      const result = service.eval(OPTIONS, RESPONSES, TYPE);
      expect(result.length).toBe(OPTIONS.length);
    }
  );

  it('each result object should contain its option and be in order', () => {
      const result = service.eval(OPTIONS, RESPONSES, TYPE);

      expect(result[0].option).toBe(OPTIONS[0]);
      expect(result[1].option).toBe(OPTIONS[1]);
    }
  );

  it('each result object should have a total of responses', () => {
      const result = service.eval(OPTIONS, RESPONSES, TYPE);

      expect(result[0].total).toBe(RESPONSES.length);
      expect(result[1].total).toBe(RESPONSES.length);
    }
  );

  it('each result object should have contains their voters', () => {
      const result = service.eval(OPTIONS, RESPONSES, TYPE);

      expect(result[0].voters.length).toBe(2);
      expect(result[0].voters).toContain('test@test.ch');
      expect(result[0].voters).toContain('test3@test.ch');
      expect(result[1].voters.length).toBe(1);
      expect(result[1].voters).toContain('test2@test.ch');
    }
  );

  it('showVoters should be false by default', () => {
      const result = service.eval(OPTIONS, RESPONSES, TYPE);

      expect(result[0].showVoter).toBe(false);
      expect(result[1].showVoter).toBe(false);
    }
  );


});
