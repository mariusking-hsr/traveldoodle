import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SurveyService} from '../survey.service';
import {combineLatest, Observable, Subscription} from 'rxjs';
import {SurveyModel} from '../survey.model';
import {ResponseModel} from '../response.model';
import {ResponseService} from '../response.service';
import {flatMap, map} from 'rxjs/operators';
import {SurveyEvaluationService} from './survey-evaluation.service';


@Component({
  selector: 'app-survey-evaluation',
  templateUrl: './survey-evaluation.component.html'
})

export class SurveyEvaluationComponent implements OnInit, OnDestroy {
  public survey$: Observable<SurveyModel>;
  public evaluatedDates;
  public evaluatedLocations;
  public evaluatedTypes;

  private response$: Observable<ResponseModel[]>;
  private subscription: Subscription;

  constructor(private route: ActivatedRoute,
              private surveyService: SurveyService,
              private responseService: ResponseService,
              private evaluationService: SurveyEvaluationService) {
  }

  ngOnInit() {

    this.survey$ = this.route.data.pipe(
      map(({survey}) => survey)
    );

    this.response$ = this.route.data.pipe(
      map(({survey}) => survey || {}),
      flatMap(survey => this.responseService.all(survey.id))
    );

    this.subscription = combineLatest<SurveyModel, ResponseModel[]>(this.survey$, this.response$)
      .subscribe((values) => {
        const survey = values[0];
        const responses = values[1];

        this.evaluatedDates = this.evaluationService.eval(survey.dates, responses, 'selectedDates');
        this.evaluatedLocations = this.evaluationService.eval(survey.locations, responses, 'selectedLocations');
        this.evaluatedTypes = this.evaluationService.eval(survey.types, responses, 'selectedTypes');
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
