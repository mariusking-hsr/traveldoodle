import {SurveyResolverService} from './survey-resolver.service';
import {routerMock, SURVEY, surveyMockService} from '../test/test-fake-objects';
import {ActivatedRouteSnapshot} from '@angular/router';
import {of} from 'rxjs';
import createSpyObj = jasmine.createSpyObj;

describe('SurveyResolverService', () => {
  let resolver: SurveyResolverService;

  beforeEach(() => {
    resolver = new SurveyResolverService(surveyMockService, routerMock);
  });

  it('should return survey observable if survey for id exists', () => {
    // given
    const route = createSpyObj('ActivatedRouteSnapshot', ['paramMap']);
    route.paramMap.get = () => 'id';

    surveyMockService.get.and.returnValue(of(SURVEY));

    // when
    const observable = resolver.resolve(route);

    // then
    observable.subscribe((survey) => {
      expect(survey).toBe(SURVEY);
    });
  });

  it('should return EMPTY observable for id not exists ', (done) => {
    // given
    const route = createSpyObj('ActivatedRouteSnapshot', ['paramMap']);
    route.paramMap.get = () => 'unknownID';

    surveyMockService.get.and.returnValue(of(undefined));

    // when
    const observable = resolver.resolve(route);

    // then
    observable.subscribe(fail, fail,  () => {
      done();
    });
  });

});
