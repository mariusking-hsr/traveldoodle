export const TRAVEL_TYPES = [
  {type: 'Roadtrip', icon: 'road'},
  {type: 'Interrail', icon: 'train'},
  {type: 'Citytrip', icon: 'archway'},
  {type: 'Worldtrip', icon: 'globe-europe' },
  {type: 'Party', icon: 'cocktail'},
  {type: 'Beach', icon: 'umbrella-beach'},
  {type: 'Spa', icon: 'spa'},
  {type: 'Cruse', icon: 'ship'},
  {type: 'Backpack', icon: 'hiking'},
  {type: 'Camping', icon: 'campground'},
  {type: 'Skiing', icon: 'skiing'},
  {type: 'Golf', icon: 'golf-ball'}
];
