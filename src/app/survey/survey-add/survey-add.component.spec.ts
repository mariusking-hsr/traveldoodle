import {SurveyAddComponent} from './survey-add.component';
import {
  CREATE_DATE_OPTION,
  CREATE_FORM,
  CREATE_LOCATION_OPTION,
  CREATE_TYPE_OPTION,
  idMockService, routerMock,
  surveyMockService
} from '../../test/test-fake-objects';
import {SurveyModel} from '../survey.model';
import {of} from 'rxjs';


describe('SurveyAddComponent', () => {
  let component: SurveyAddComponent;

  beforeEach(() => {
    component = new SurveyAddComponent(routerMock, idMockService, surveyMockService, null);
    component.survey = {
      dates: [],
      locations: [],
      types: []
    };
  });

  it(`createSurvey should call surveyService.create`, () => {
    const createSpy = surveyMockService.create.and.returnValue(of({}));

    component.createSurvey(<SurveyModel>{});

    expect(createSpy.calls.any()).toBe(true);
    expect(routerMock.navigate.calls.any()).toBe(true);
  });

  it(`onSubmit should call createSurvey`, (done) => {
    component.createSurvey = () => {
      done();
    };

    component.onSubmit(CREATE_FORM);
  });

  it(`onSubmit merges this.survey and ngForm.value an pass it to createSurvey`, (done) => {
    component.survey = {
      dates: [CREATE_DATE_OPTION],
      locations: [CREATE_LOCATION_OPTION],
      types: [CREATE_TYPE_OPTION]
    };

    component.createSurvey = (survey) => {
      expect(survey.title).toBe('title');
      expect(survey.description).toBe('description');

      expect(survey.dates.length).toBe(1);
      expect(survey.dates).toContain(CREATE_DATE_OPTION);

      expect(survey.locations.length).toBe(1);
      expect(survey.locations).toContain(CREATE_LOCATION_OPTION);

      expect(survey.types.length).toBe(1);
      expect(survey.types).toContain(CREATE_TYPE_OPTION);
      done();
    };

    component.onSubmit(CREATE_FORM);
  });

  it(`onDatesSelected should add type to survey.dates`, () => {
    component.onDatesSelected({});

    expect(component.survey.dates.length).toBe(1);
  });

  it(`onRemoveDate should remove type to survey.dates`, () => {
    component.survey.dates = [CREATE_DATE_OPTION];

    component.onRemoveDate(CREATE_DATE_OPTION.id);

    expect(component.survey.dates.length).toBe(0);
  });

  it(`onLocationSelected should add type to survey.locations`, () => {
    component.onLocationSelected({});

    expect(component.survey.locations.length).toBe(1);
  });

  it(`onRemoveLocation should remove type to survey.locations`, () => {
    component.survey.locations = [CREATE_LOCATION_OPTION];

    component.onRemoveLocation(CREATE_LOCATION_OPTION.id);

    expect(component.survey.locations.length).toBe(0);
  });

  it(`onTypeSelected should add type to survey.types`, () => {
    component.onTypeSelected({});

    expect(component.survey.types.length).toBe(1);
  });

  it(`onTypeUnselected should remove type to survey.types`, () => {
    component.survey.types = [CREATE_TYPE_OPTION];

    component.onTypeUnselected(CREATE_TYPE_OPTION.id);

    expect(component.survey.types.length).toBe(0);
  });

});
