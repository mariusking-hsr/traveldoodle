import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {SurveyService} from '../survey.service';
import {AuthService} from '../../auth/auth.service';
import {IdService} from '../../shared/utils/id.service';
import {TRAVEL_TYPES} from '../default-travel-types';
import {DateOption, LocationOption, SurveyModel, TypeOption} from '../survey.model';
import {Subscription} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-survey-add',
  templateUrl: './survey-add.component.html'
})
export class SurveyAddComponent implements OnInit, OnDestroy {

  public travelTypes;
  public survey;

  private subscription: Subscription;

  constructor(
    private router: Router,
    private idService: IdService,
    private surveyService: SurveyService,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.travelTypes = TRAVEL_TYPES.map(type => {
      return Object.assign(type, {
        id: this.idService.generateId(),
        selected: false
      });
    });
    this.survey = {
      dates: [],
      locations: [],
      types: []
    };
    this.subscription = this.authService.currentUser$.subscribe(({uid}) => {
      this.survey.userId = uid;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public onSubmit({value}: NgForm) {
    const survey: SurveyModel = Object.assign(this.survey, value);
    this.createSurvey(survey);
  }

  public createSurvey(survey: SurveyModel) {
    this.surveyService.create(survey)
      .pipe(first())
      .subscribe(() => {
        this.router.navigate([`survey/${survey.id}`]);
      });
  }

  public onDatesSelected(event) {
    this.survey.dates.push({
      id: this.idService.generateId(),
      from: event[0],
      to: event[1]
    });
  }

  public onRemoveDate(dateId) {
    this.survey.dates = this.survey.dates.filter((date: DateOption) => date.id !== dateId);
  }

  public onLocationSelected(event) {
    this.survey.locations.push({
      id: this.idService.generateId(),
      location: event
    });
  }

  public onRemoveLocation(locationId) {
    this.survey.locations = this.survey.locations.filter((location: LocationOption) => location.id !== locationId);
  }

  public onTypeSelected(type) {
    this.survey.types.push({...type});
  }

  public onTypeUnselected(typeId) {
    this.survey.types = this.survey.types.filter((type: TypeOption) => type.id !== typeId);
  }
}
