import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {SURVEY, SurveyModel} from './survey.model';
import {Observable} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {IdService} from '../shared/utils/id.service';
import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  constructor(private readonly afs: AngularFirestore,
              private idService: IdService) {
  }

  public create(survey: SurveyModel): Observable<void> {
    survey.id = this.idService.generateId();
    survey.createdAt = Timestamp.now();

    return fromPromise(
      this.afs.collection<SurveyModel>(SURVEY).doc(survey.id).set(survey)
    );
  }

  public get(id: string): Observable<SurveyModel> {
    const document = this.afs.doc<SurveyModel>(`${SURVEY}/${id}`);
    return document.valueChanges();
  }
}
