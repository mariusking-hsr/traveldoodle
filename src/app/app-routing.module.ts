import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {SurveyAddComponent} from './survey/survey-add/survey-add.component';
import {SurveyVoteComponent} from './survey/survey-vote/survey-vote.component';
import {SigninComponent} from './auth/signin/signin.component';
import {SignupComponent} from './auth/signup/signup.component';
import {SurveyComponent} from './survey/survey.component';
import {SurveyEvaluationComponent} from './survey/survey-evaluation/survey-evaluation.component';
import {AuthGuard} from './auth/auth.guard';
import {SurveyResolverService} from './survey/survey-resolver.service';
import {NotFoundComponent} from './not-found/not-found.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: '404', component: NotFoundComponent},
  {path: 'signin', component: SigninComponent},
  {path: 'signup', component: SignupComponent},
  {
    path: 'survey', component: SurveyComponent, children: [
      {path: 'create', component: SurveyAddComponent, canActivate: [AuthGuard]},
      {
        path: ':id',
        component: SurveyVoteComponent,
        resolve: {
          survey: SurveyResolverService
        }
      },
      {
        path: ':id/eval',
        component: SurveyEvaluationComponent,
        resolve: {
          survey: SurveyResolverService
        }
      },
    ]
  },
  {path: 'create', component: SurveyAddComponent},
  {path: 'vote', component: SurveyVoteComponent},
  {path: '**', redirectTo: '/404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
