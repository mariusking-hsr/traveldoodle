import {Component} from '@angular/core';
import {AuthService} from '../auth.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent {

  constructor(private authSerivce: AuthService) {
  }

  public signin({value: user}: NgForm) {
    this.authSerivce.signin(user);
  }
}
