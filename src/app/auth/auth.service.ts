import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import sha256 from 'crypto-js/sha256';
import Base64 from 'crypto-js/enc-base64';
import {AuthUserModel} from './auth-user.model';
import {Observable} from 'rxjs';
import {User} from 'firebase/app';
import {Router} from '@angular/router';
import {ErrorHandlerService} from '../shared/error/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public currentUser$: Observable<User>;

  private redirectUrl = '/home';

  constructor(private afAuth: AngularFireAuth,
              private router: Router,
              private errorHandler: ErrorHandlerService) {

    this.currentUser$ = this.afAuth.user;
  }

  private static _hash(stringToHash: string): string {
    return Base64.stringify(sha256(stringToHash));
  }

  public async signin(user: AuthUserModel) {
    try {
      await this.afAuth.auth.signInWithEmailAndPassword(user.email, AuthService._hash(user.password));
      this.redirect();
    } catch (e) {
      this.errorHandler.handleError(e);
    }
  }

  public async signup(user: AuthUserModel) {
    try {
      await this.afAuth.auth.createUserWithEmailAndPassword(user.email, AuthService._hash(user.password));
      this.redirect();
    } catch (e) {
      this.errorHandler.handleError(e);
    }
  }

  public signout() {
    this.afAuth.auth.signOut();
  }

  public setRedirectUrl(redirectUrl: string) {
    this.redirectUrl = redirectUrl;
  }

  private redirect() {
    this.router.navigate([this.redirectUrl]);
  }
}
