import {NgForm} from '@angular/forms';
import {DateOption, LocationOption, SurveyModel, TypeOption} from '../survey/survey.model';
// @ts-ignore
import createSpyObj = jasmine.createSpyObj;

export const CREATE_FORM = <NgForm>{
  value: {
    title: 'title',
    description: 'description'
  }
};

export const CREATE_DATE_OPTION = <DateOption>{id: 'dateId'};
export const CREATE_LOCATION_OPTION = <LocationOption>{id: 'locationId'};
export const CREATE_TYPE_OPTION = <TypeOption>{id: 'typeId'};

export const SURVEY = <SurveyModel>{id: 'surveyId'};

export const OPTIONS = [{
  'from': {'seconds': 1547679600, 'nanoseconds': 0},
  'id': '2p5zcIf4eP2KLwAFt9ZB',
  'to': {'seconds': 1547766000, 'nanoseconds': 0}
}, {
  'from': {'seconds': 1548284400, 'nanoseconds': 0},
  'id': 'KsCFx1FHxCdZe5ALl8vT',
  'to': {'seconds': 1548370800, 'nanoseconds': 0}
}];

export const RESPONSES = [{
  'createdAt': {'seconds': 1547305952, 'nanoseconds': 956000000},
  'email': 'test3@test.ch',
  'id': 'PI6kF5ayhc3dfrLlU8v2',
  'selectedDates': ['2p5zcIf4eP2KLwAFt9ZB'],
  'selectedLocations': ['GUWbeeR6cdDSJgNp6S5P'],
  'selectedTypes': ['GUvAWN1yX8SGHdCSyoIk']
}, {
  'createdAt': {'seconds': 1547305982, 'nanoseconds': 609000000},
  'email': 'test2@test.ch',
  'id': 'W4sy1ycV9c0Lwsku9GFl',
  'selectedDates': ['KsCFx1FHxCdZe5ALl8vT'],
  'selectedLocations': '',
  'selectedTypes': ''
}, {
  'createdAt': {'seconds': 1547305968, 'nanoseconds': 786000000},
  'email': 'test@test.ch',
  'id': 'tHuqnaglhuzhsIVNemIL',
  'selectedDates': ['2p5zcIf4eP2KLwAFt9ZB'],
  'selectedLocations': ['tNQH378JHPBausVaCHhN'],
  'selectedTypes': ['u79zOGTkWp3LP2PCLMz2']
}];

export const TYPE = 'selectedDates';

export const idMockService = createSpyObj('IdMockService', ['generateId']);
export const surveyMockService = createSpyObj('SurveyMockService', ['create', 'get']);

export const routerMock = createSpyObj('RouterMock', ['navigate']);
