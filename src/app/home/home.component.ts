import {Component} from '@angular/core';
import {InfoDialogComponent} from './info-dialog/info-dialog.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  constructor(public dialog: MatDialog) {
  }

  openInfoDialog(): void {
    this.dialog.open(InfoDialogComponent, {
      width: '512px'
    });
  }
}
