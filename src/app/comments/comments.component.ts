import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommentModel} from './comment.model';
import {Observable, Subscription} from 'rxjs';
import {AuthService} from '../auth/auth.service';
import {ActivatedRoute, Params} from '@angular/router';
import {CommentService} from './comment.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit, OnDestroy {

  public comments$: Observable<CommentModel[]>;

  private subscription: Subscription;

  constructor(private commentService: CommentService,
              private authService: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription = this.route.params.subscribe((params: Params) => {
      this.comments$ = this.commentService.all(params.id);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
