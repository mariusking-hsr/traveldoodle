import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {CommentModel} from '../comment.model';
import {CommentService} from '../comment.service';
import {AuthService} from '../../auth/auth.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {User} from 'firebase';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit, OnDestroy {

  public currentUser$: Observable<User>;

  private surveyId: string;
  private surveyComment;
  private subscriptions = new Array<Subscription>();

  constructor(private commentService: CommentService,
              private authService: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.surveyComment = {};
    this.currentUser$ = this.authService.currentUser$;

    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        this.surveyId = params.id;
      }),
      this.authService.currentUser$.subscribe((user: User) => {
        if (user) {
          this.surveyComment.email = user.email;
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  public onSubmit(form: NgForm) {
    const surveyComment: CommentModel = Object.assign(this.surveyComment, form.value);
    this.createComment(this.surveyId, surveyComment);
    form.reset();
  }

  private createComment(surveyId: string, surveyComment: CommentModel) {
    this.commentService.add(surveyId, surveyComment);
  }

}
