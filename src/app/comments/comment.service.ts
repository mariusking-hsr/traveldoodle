import {Injectable} from '@angular/core';
import {COMMENT, CommentModel} from './comment.model';
import {Observable} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {ResponseModel} from '../survey/response.model';
import {SURVEY} from '../survey/survey.model';
import {AngularFirestore} from '@angular/fire/firestore';
import {IdService} from '../shared/utils/id.service';
import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private readonly afs: AngularFirestore,
              private idService: IdService) { }

  public add(surveyId: string, comment: CommentModel): Observable<void> {
    comment.id = this.idService.generateId();
    comment.createdAt = Timestamp.now();

    return fromPromise(
      this.afs.collection<ResponseModel>(`${SURVEY}/${surveyId}/${COMMENT}`).doc(comment.id).set(comment)
    );
  }

  public all(surveyId: string): Observable<CommentModel[]> {
    const collection = this.afs.collection<CommentModel>(
      `${SURVEY}/${surveyId}/${COMMENT}`,
      ref => ref.orderBy('createdAt', 'desc')
    );
    return collection.valueChanges();
  }
}
