import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;

export class CommentModel {
  id: string;
  comment: string;
  email: string;
  createdAt: Timestamp;
}

export const COMMENT = 'comments';
