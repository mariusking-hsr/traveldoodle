import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from 'firebase';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public currentUser$: Observable<User>;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.currentUser$ = this.authService.currentUser$;
  }

  signout() {
    this.authService.signout();
    this.router.navigate(['/']);
  }
}
