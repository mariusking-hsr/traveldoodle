import {Injectable, NgZone} from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackBar: MatSnackBar,
              private zone: NgZone) {
  }

  public popUp(message: string): void {
    this.zone.run(() => {
      this.snackBar.open(message, '', {
        duration: 5000
      });
    });
  }
}
