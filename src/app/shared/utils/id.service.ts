import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class IdService {

  constructor(private readonly afs: AngularFirestore) {
  }

  public generateId(): string {
    return this.afs.createId();
  }
}
