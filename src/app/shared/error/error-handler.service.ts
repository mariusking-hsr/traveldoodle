import {ErrorHandler, Injectable} from '@angular/core';
import {ErrorLoggerService} from './error-logger.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService extends ErrorHandler {

  constructor(public errorLogger: ErrorLoggerService) {
    super();
  }

  public handleError(exception: any) {
    if (exception.message && exception.message.length > 0) {
      this.errorLogger.log(exception.message);
    } else {
      this.errorLogger.log('oops something went wrong!');
    }

    super.handleError(exception);
  }

}
