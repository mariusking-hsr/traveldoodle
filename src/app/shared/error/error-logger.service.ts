import {Injectable} from '@angular/core';
import {SnackbarService} from '../utils/snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorLoggerService {

  constructor(private snackbarService: SnackbarService) {
  }

  public log(message: string): void {
    this.snackbarService.popUp(message);
  }
}
