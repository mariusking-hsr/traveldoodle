import {Component, EventEmitter, Output} from '@angular/core';
import {google} from '@agm/core/services/google-maps-types';
import PlaceResult = google.maps.places.PlaceResult;

@Component({
  selector: 'app-places-autocomplete',
  templateUrl: './places-autocomplete.component.html',
  styleUrls: ['./places-autocomplete.component.scss']
})
export class PlacesAutocompleteComponent {

  public types = ['(regions)'];
  public country = [];

  @Output()
  public placeSelected = new EventEmitter<string>();

  public onPlaceSelected(result: PlaceResult, inputElm) {
    this.placeSelected.emit(result.formatted_address);
    inputElm.value = '';
  }

}
