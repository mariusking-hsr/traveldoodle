import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TypeOption} from '../../../../survey/survey.model';

@Component({
  selector: 'app-selectable-icon-option',
  templateUrl: './selectable-icon-option.component.html',
  styleUrls: ['./selectable-icon-option.component.scss']
})
export class SelectableIconOptionComponent {

  @Input()
  option: TypeOption;

  @Output()
  selected = new EventEmitter();

  @Output()
  unselected = new EventEmitter();

  onToggle() {
    this.option.selected = !this.option.selected;

    if (this.option.selected) {
      this.selected.emit(this.option);
    } else {
      this.unselected.emit(this.option.id);
    }
  }

}
