import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Option} from '../../../../survey/survey.model';

@Component({
  selector: 'app-deleteable-icon-option',
  templateUrl: './deleteable-icon-option.component.html',
  styleUrls: ['./deleteable-icon-option.component.scss']
})
export class DeleteableIconOptionComponent {

  @Input()
  option: Option;

  @Output()
  remove = new EventEmitter();

  onRemove() {
    this.remove.emit(this.option.id);
  }
}
