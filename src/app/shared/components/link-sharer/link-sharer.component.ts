import {Component, Inject} from '@angular/core';
import {ClipboardService} from 'ngx-clipboard';
import {SnackbarService} from '../../utils/snackbar.service';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'app-link-sharer',
  templateUrl: './link-sharer.component.html',
  styleUrls: ['./link-sharer.component.scss']
})
export class LinkSharerComponent {

  constructor(
    @Inject(DOCUMENT) document,
    private clipboard: ClipboardService,
    private snackbarService: SnackbarService
  ) { }

  public copyUrlToClipboard() {
    this.clipboard.copyFromContent(document.URL);
    this.snackbarService.popUp('copy url to clipboard');
  }

  public shareOnWhatsapp() {
    document.location.href = `whatsapp://send?text=join me on a trip ${document.URL}`;
  }
}
