import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-label-with-icon',
  templateUrl: './label-with-icon.component.html',
  styleUrls: ['./label-with-icon.component.scss']
})
export class LabelWithIconComponent {

  @Input()
  icon: String | Array<String>;

}
