import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getNavigation() {
    return element.all(by.css('app-root header button'));
  }
}
